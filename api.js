const express = require("express")
const bodyParser = require("body-parser")

const app = express()

// CONFIGURACION: CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
  
    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  
    app.options('*', (req, res) => {
      // allowed XHR methods  
      res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
      res.send();
    });
  });
  
  // CONFIGURACION: traducimos el json
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  app.get("/api/frutas", (req, res) => {
      
        res.status(200).send({
            success: "true",
            message: "Frutas"
        })
  })

const PORT = 5000;
app.listen(PORT, function () {
  console.log(`API fruteria corriendo en puerto ${PORT}`);
});