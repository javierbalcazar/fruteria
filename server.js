const http = require("http");

const server = http.createServer((request, response) => {
    response.writeHead(200, {"content-Type": "text/html"});
    response.write("<h1>Servidor/h1>");
    response.end();

});
const host = "localhost"
const port = "4000"

server.listen(port, host, () => {
    console.log(`Servidor 1 iniciado en el puerto http://${host}:${port}`);
});
